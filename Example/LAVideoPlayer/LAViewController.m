//
//  LAViewController.m
//  LAVideoPlayer
//
//  Created by Stéphane Couzinier on 10/30/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

#import "LAViewController.h"
#import "LAVideoPlayerFullScreenControls.h"

#import <LAVideoPlayer/LAVideoFullScreenViewController.h>
#import <LAVideoPlayer/NSBundle+PodBundle.h>
//org.cocoapods.demo.LAVideoPlayer-Example
//recup des texte dans un bundle
#define LocalizedString(stringKey,tableName)        [[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"LAVideoPlayer" ofType:@"bundle"]] localizedStringForKey:stringKey value:stringKey table:tableName]

@interface LAViewController ()

@end

@implementation LAViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - Action Methods

- (IBAction)didTouchOpenFramework:(id)sender
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoDidComplete:) name:@"videoDidComplete" object:nil];
    
    NSDictionary * tmp = [[NSDictionary alloc]initWithObjectsAndKeys:@"",@"",nil];
    
    LAVideoFullScreenViewController * vc = [[LAVideoFullScreenViewController alloc] initWithVideoUrl:@"http://video.doctissimo.fr/4/5/6/31456/hd-31456.mp4" andAds:@"https://pubads.g.doubleclick.net/gampad/ads?sz=1920x1080&iu=/12271007/app_smartphones_parismatch_android_ios_s136091/preroll_video_rg_r168115/f168117_d321video_pre_roll_1_preroll_videos_16_9_1920x1080&ciu_szs&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&m_ast=vast&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]&overlay=0" andGaAction:@"" andGaLabel:@"" andGaDimensions:tmp andATSiteID:@"5" withArticle:TRUE];
  
    [self presentViewController:vc animated:YES completion:^{
        //Player images custom
        LAVideoPlayerFullScreenControls * controls = [vc getControls];
        [controls setBtnSliderImage:[UIImage imageNamed:@"player_now"]];
        [controls setBtnPlayImage:[UIImage imageNamed:@"player_play"]];
        [controls.progressLabel setFont:[UIFont boldSystemFontOfSize:10]];
        [controls.durationLabel setFont:[UIFont boldSystemFontOfSize:10]];
    }];
}

- (void)videoDidComplete:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:@"videoDidComplete" object:nil];

    NSLog(@"videoDidComplete");
}

#pragma mark -  Memory Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
