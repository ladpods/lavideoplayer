//
//  LAModalVideoViewController.m
//  LAVideoPlayerSample
//
//  Created by MIRAULT Fabien on 04/07/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAModalVideoViewController.h"
#import "LAVideoFullScreenViewController.h"
#import "LAVideoPlayerConstant.h"

//----------------------------------------------
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
//----------------------------------------------

@interface LAModalVideoViewController ()

@property (nonatomic, strong) IBOutlet UIButton *videoButton;

@end

@implementation LAModalVideoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0") && IS_IPAD)
    {
        [self.videoButton setFrame:CGRectMake(self.videoButton.frame.origin.x, self.videoButton.frame.origin.y - 44, self.videoButton.frame.size.width, self.videoButton.frame.size.height - 20)];
    }
}

- (IBAction)handleVideo:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    [self presentViewController:[[LAVideoFullScreenViewController alloc] init] animated:YES completion:^{
        
    }];
}

#pragma clang diagnostic pop
@end