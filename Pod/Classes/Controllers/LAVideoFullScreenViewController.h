//
//  LAVideoFullScreenViewController.h
//  LAVideoPlayerSample
//
//  Created by MIRAULT Fabien on 04/07/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LAVideoPlayerFullScreenControls;

@interface LAVideoFullScreenViewController : UIViewController
{
}

// -------------------------------------------------

- (id)initWithVideoUrl:(NSString *)videoUrl andAds:(NSString *)adsUrl andGaAction:(NSString *) gaAction andGaLabel:(NSString *) gaLabel andGaDimensions:(NSDictionary *) gaDimensions andATSiteID:(NSString *) ATSiteID;

- (id)initWithVideoUrl:(NSString *)videoUrl andAds:(NSString *)adsUrl andGaAction:(NSString *) gaAction andGaLabel:(NSString *) gaLabel andGaDimensions:(NSDictionary *) gaDimensions andATSiteID:(NSString *) ATSiteID withArticle:(BOOL)shouldDisplayArticle;

// -------------------------------------------------
- (void) handleClose;

// -------------------------------------------------
- (void)setCloseButtonImage:(UIImage*)image;

// -------------------------------------------------
- (LAVideoPlayerFullScreenControls*)getControls;


@end