//
//  LAVideoFullScreenViewController.m
//  LAVideoPlayerSample
//
//  Created by MIRAULT Fabien on 04/07/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoFullScreenViewController.h"
#import "LAVideoPlayerFullScreenControls.h"
#import "LAVideoPlayerCustom.h"
#import "LADUtils.h"
#import "NSBundle+PodBundle.h"

#import <TAGManager.h>
#import <TAGDataLayer.h>

//----------------------------------------------

@interface LAVideoFullScreenViewController () <LAVideoPlayerDelegate>

@property (nonatomic, assign) BOOL keepVideoPlayer;

@property (nonatomic, strong) LAVideoPlayerCustom *videoPlayer;
@property (nonatomic, strong) UIButton            *closeButton;
@property (nonatomic, strong) NSString            *videoUrl;
@property (nonatomic, strong) NSString            *adsUrl;
@property (nonatomic, strong) NSString            *gaAction;
@property (nonatomic, strong) NSString            *gaLabel;
@property (nonatomic, strong) NSString            *ATSiteID;
@property (nonatomic, strong) NSDictionary        *gaDimensions;
@property (nonatomic, assign) BOOL                shouldDisplayArticle;

@end

@implementation LAVideoFullScreenViewController

- (id)initWithVideoUrl:(NSString *)videoUrl andAds:(NSString *)adsUrl andGaAction:(NSString *) gaAction andGaLabel:(NSString *) gaLabel andGaDimensions:(NSDictionary *) gaDimensions andATSiteID:(NSString *) ATSiteID withArticle:(BOOL)shouldDisplayArticle
{
    self = [super init];
    
    if (self)
    {
        self.videoUrl = videoUrl;
        self.adsUrl = adsUrl;
        
        // Google Analytics parameters | check they aren't nil
        if (gaAction != nil)
        {
            self.gaAction = gaAction;
        }
        else
        {
            self.gaAction = @"";
        }
        
        if (gaLabel != nil)
        {
            self.gaLabel=gaLabel;
        }
        else
        {
            self.gaLabel = @"";
        }
        
        if (gaDimensions != nil)
        {
            self.gaDimensions = gaDimensions;
        }
        else
        {
            self.gaDimensions = [[NSDictionary alloc]initWithObjectsAndKeys:@"",@"",nil];
        }
        
        if (ATSiteID != nil)
        {
            self.ATSiteID = ATSiteID;
        }
        else
        {
            self.ATSiteID = @"";
        }
        
        self.shouldDisplayArticle = shouldDisplayArticle;
    }
    
    return self;
}

- (id)initWithVideoUrl:(NSString *)videoUrl andAds:(NSString *)adsUrl andGaAction:(NSString *) gaAction andGaLabel:(NSString *) gaLabel andGaDimensions:(NSDictionary *) gaDimensions andATSiteID:(NSString *) ATSiteID
{
    return [self initWithVideoUrl:videoUrl andAds:adsUrl andGaAction:gaAction andGaLabel:gaLabel andGaDimensions:gaDimensions andATSiteID:ATSiteID withArticle:false];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.videoPlayer = [[LAVideoPlayerCustom alloc] initWithFrame:[LADUtils getScreenRect]];
    [self.videoPlayer setControlsClass:[LAVideoPlayerFullScreenControls class]];
    [self.videoPlayer setAutoHideControls:YES];
    [self.videoPlayer setDelegate:self];
    
    [self.view addSubview:self.videoPlayer];
    
    UIImage *closeImage = [NSBundle imageNamed:@"IconClose"];
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:closeImage forState:UIControlStateNormal];
    [self.closeButton setFrame:CGRectMake(15, 15, closeImage.size.width, closeImage.size.height)];
    [self.closeButton addTarget:self action:@selector(handleClose) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton setHidden:YES];
    
    [self.view addSubview:self.closeButton];
    
    if (self.videoUrl) {
        [self.videoPlayer playVideoWithUrl:self.videoUrl andAdsTag:self.adsUrl andGaAction:self.gaAction andGaLabel:self.gaLabel andGaDimensions:self.gaDimensions];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (!self.keepVideoPlayer)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"videoDidComplete" object:nil];
        [self.videoPlayer clear];
    }
    else
    {
        TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
        [dataLayer push:@{@"event": @"videoStop", @"rubrique": self.gaAction, @"page": self.gaLabel, @"type": @"", @"siteId": self.ATSiteID}];
    }
}

#pragma mark - Set Custom image

- (void)setCloseButtonImage:(UIImage*)image
{
    [self.closeButton setImage:image forState:UIControlStateNormal];
}

#pragma mark - getters
- (LAVideoPlayerFullScreenControls*)getControls
{
    return [self.videoPlayer getControls];
}

#pragma mark - Actions

- (void)handleClose
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - LAVideoPlayer Delegate

- (void)adWillOpenInAppBrowser
{
    self.keepVideoPlayer = YES;
}

- (void)adWillCloseInAppBrowser
{
    self.keepVideoPlayer = NO;
}

- (void)adDidStart
{
    [self.closeButton setHidden:NO];
}

- (void)videoDidStart
{
    [self.closeButton setHidden:YES];
    
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"videoStart", @"rubrique": self.gaAction, @"page": self.gaLabel,
                      @"type": @"", @"siteId": self.ATSiteID,@"length":[NSString stringWithFormat:@"%lf",self.videoPlayer.getItemDuration]}];
    
}

- (void)videoDidStop
{
    [self.closeButton setHidden:YES];
    
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"videoStop", @"rubrique": self.gaAction, @"page": self.gaLabel,
                      @"type": @"", @"siteId": self.ATSiteID,@"length":[NSString stringWithFormat:@"%lf",self.videoPlayer.getItemDuration]}];
}
- (void)videoDidPause
{
    [self.closeButton setHidden:YES];
    
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    
    if(self.videoPlayer.isPlaying ==true)
    {
        [dataLayer push:@{@"event": @"videoStop", @"rubrique": self.gaAction, @"page": self.gaLabel,
                          @"type": @"", @"siteId": self.ATSiteID,@"length":[NSString stringWithFormat:@"%lf",self.videoPlayer.getItemDuration]}];
    }
}

- (void)videoDidComplete
{
    if(self.shouldDisplayArticle)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"videoDidComplete" object:self];
    }
    else
    {
        [self handleClose];
    }
}

@end