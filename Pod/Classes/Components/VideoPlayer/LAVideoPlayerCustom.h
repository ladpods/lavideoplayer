//
//  VPVideoPlayer.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 17/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAVideoPlayerControlsProtocol.h"
#import <CoreMedia/CoreMedia.h>

// -------------------------------------------------

@protocol LAVideoPlayerDelegate <NSObject>

@optional

- (void)adDidStart;
- (void)adDidComplete;
- (void)adWillOpenInAppBrowser;
- (void)adWillCloseInAppBrowser;
- (void)videoDidStart;
- (void)videoDidPause;
- (void)videoDidStop;
- (void)videoDidProgress:(CGFloat)progress;
- (void)videoWillExpand;
- (void)videoDidExpand;
- (void)videoWillContract;
- (void)videoDidContract;
- (void)videoDidComplete;

@end

// -------------------------------------------------

@interface LAVideoPlayerCustom : UIView

@property (nonatomic, assign  ) id<LAVideoPlayerDelegate> delegate;
@property (nonatomic, assign  ) BOOL                  autoHideControls;
@property (nonatomic, readonly) BOOL                  isReadyForPlaying;

// -------------------------------------------------

- (void)setControlsClass:(Class)controlsClass;
- (void)refreshWithFrame:(CGRect)newFrame;
- (void)refreshWithFrameWithAnimation:(CGRect)newFrame;
- (void)playVideoWithUrl:(NSString *)videoUrl andAdsTag:(NSString *)adsTag andGaAction:(NSString *) gaAction andGaLabel:(NSString *) gaLabel andGaDimensions: (NSDictionary *) gaDimensions;
- (void)pause;
- (void)resume;
- (void)seekToTime:(CMTime)time;
- (void)clear;
- (void)changeVolume:(CGFloat)volume;

- (BOOL)isPlaying;
- (CGFloat)getItemDuration;
- (UIView<LAVideoPlayerControlsProtocol> *)getControls;

// Chromecast
- (void)showDevicesSelectionForView:(UIView *)originView;
- (BOOL)isConnectedToChromecast;
- (NSArray *)chromecastDevices;
- (NSString *)gaAction;
- (NSString *)gaLabel;
- (NSDictionary *)gaDimensions;

@end