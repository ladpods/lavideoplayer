//
//  NSBundle+PodBundle.m
//  LAVideoPlayer
//
//  Created by BELLEROSE Odile on 26/10/2015.
//  Copyright © 2015 BELLEROSE Odile. All rights reserved.
//
#import "LAVideoPlayerConstant.h"
#import "NSBundle+PodBundle.h"


@implementation NSBundle (PodBundle)

+ (instancetype)MYBundle
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *bundleUrl = [mainBundle URLForResource:@"LAVideoPlayer" withExtension:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithURL:bundleUrl];
    
    return bundle;
}

+ (UIImage*)imageNamed:(NSString*)name
{
    UIImage *image;
    
    image = [UIImage imageNamed:name];
    
    if (image)
    {
        return image;
    }
    
    image = [UIImage imageWithContentsOfFile:[[[NSBundle MYBundle] resourcePath] stringByAppendingPathComponent:name]];
    
    return image;
}

@end
