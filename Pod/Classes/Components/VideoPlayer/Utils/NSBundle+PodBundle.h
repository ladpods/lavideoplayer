//
//  NSBundle+PodBundle.h
//  LAVideoPlayer
//
//  Created by BELLEROSE Odile on 26/10/2015.
//  Copyright © 2015 BELLEROSE Odile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (PodBundle)
+ (instancetype)MYBundle;
+ (UIImage*)imageNamed:(NSString*)name;
@end
