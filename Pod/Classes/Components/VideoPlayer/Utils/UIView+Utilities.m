//
//  UIView+Utilities.m
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 17/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "UIView+Utilities.h"

@implementation UIView (Utilities)

- (UIViewController *)firstAvailableUIViewController
{    
    return (UIViewController *)[self traverseResponderChainForUIViewController];
}

- (id)traverseResponderChainForUIViewController
{
    id nextResponder = [self nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
    {
        return nextResponder;
    }
    else if ([nextResponder isKindOfClass:[UIView class]])
    {
        return [nextResponder traverseResponderChainForUIViewController];
    }
    else
    {
        return nil;
    }
}

@end