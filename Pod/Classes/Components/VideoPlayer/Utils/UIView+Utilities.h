//
//  UIView+Utilities.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 17/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utilities)

- (UIViewController *)firstAvailableUIViewController;

@end