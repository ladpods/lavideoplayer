//
//  LASlider.m
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 24/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LASlider.h"

// -------------------------------------------------

@interface LASlider ()

@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, strong) UIImageView *trackingProgressView;
@property (nonatomic, strong) UIImageView *bufferedProgressView;
@property (nonatomic, strong) UIButton    *toggleButton;

@end

// -------------------------------------------------

@implementation LASlider

#pragma mark - LifeCycle

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    self.bufferedFrom = 0.0;
    self.value        = 0.0;
    self.bufferedTo   = 0.0;
    
    CGRect frame = self.bounds;
    frame.size.height = 4 ;
    
    self.backgroundView = [[UIImageView alloc] initWithFrame:frame];
    self.backgroundView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.backgroundView];
    
    self.bufferedProgressView = [[UIImageView alloc] initWithFrame:frame];
    self.bufferedProgressView.userInteractionEnabled = YES;
    [self addSubview:self.bufferedProgressView];

    self.trackingProgressView = [[UIImageView alloc] initWithFrame:frame];
    [self addSubview:self.trackingProgressView];
    
    self.toggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.toggleButton setAdjustsImageWhenHighlighted:NO];
    [self.toggleButton addTarget:self action:@selector(handleDragStartDragging:)forControlEvents:UIControlEventTouchDown];
    [self.toggleButton addTarget:self action:@selector(handleDrag:withEvent:)forControlEvents:UIControlEventTouchDragOutside|UIControlEventTouchDragInside];
    [self.toggleButton addTarget:self action:@selector(handleToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.toggleButton];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.backgroundImage)
    {
        [self.backgroundView setFrame:CGRectMake(0, self.frame.size.height/2- self.backgroundView.frame.size.height/2, self.frame.size.width, self.backgroundView.frame.size.height)];
    }
    
    if (self.toggleImage)
    {
        NSInteger buttonOffset = 10;
        [self.toggleButton setFrame:CGRectMake(self.value * ((self.frame.size.width + buttonOffset) - self.toggleButton.frame.size.width) - buttonOffset / 2,
                                               (self.frame.size.height - self.toggleButton.frame.size.height) / 2,
                                               self.toggleButton.frame.size.width,
                                               self.toggleButton.frame.size.height)];
    }
    
    if (self.bufferedImage)
    {
        [self.bufferedProgressView setFrame:CGRectMake(self.toggleButton.frame.origin.x + self.toggleButton.frame.size.width / 2,
                                                       (self.frame.size.height - self.bufferedProgressView.frame.size.height) / 2,
                                                       self.bufferedTo * (self.frame.size.width - self.toggleButton.frame.size.width / 2) - self.toggleButton.frame.origin.x,
                                                       self.bufferedProgressView.frame.size.height)];
    }
    
    if (self.trackingImage)
    {
        [self.trackingProgressView setFrame:CGRectMake(0,
                                                       (self.frame.size.height - self.trackingProgressView.frame.size.height) / 2,
                                                       self.value * (self.frame.size.width - self.toggleButton.frame.size.width / 2) + self.toggleButton.frame.size.width / 2,
                                                       self.trackingProgressView.frame.size.height)];
    }
}

#pragma mark - Actions

- (void)handleDragStartDragging:(UIButton *)button
{
    [self sendActionsForControlEvents:UIControlEventTouchDragEnter];
}

- (void)handleDrag:(UIButton *)button withEvent:(UIEvent *)event
{
    CGPoint pointInView = [[[event allTouches] anyObject] locationInView:self];
    CGFloat finalPosX = MIN(self.frame.size.width - self.toggleButton.frame.size.width / 2, MAX(pointInView.x, self.toggleButton.frame.size.width / 2)) - self.toggleButton.frame.size.width / 2;
    self.value = finalPosX / (self.frame.size.width - self.toggleButton.frame.size.width);
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)handleToggle:(UIButton *)button
{
    [self sendActionsForControlEvents:UIControlEventTouchDragExit];
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint tapPoint = [recognizer locationInView:self];
    CGFloat finalPosX = MIN(self.frame.size.width - self.toggleButton.frame.size.width / 2, MAX(tapPoint.x, self.toggleButton.frame.size.width / 2)) - self.toggleButton.frame.size.width / 2;
    self.value = finalPosX / (self.frame.size.width - self.toggleButton.frame.size.width);
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark - Setters

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    _backgroundImage = backgroundImage;
    [self.backgroundView setImage:self.backgroundImage];
    [self.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    [self setNeedsLayout];
}

- (void)setTrackingImage:(UIImage *)trackingImage
{
    _trackingImage = trackingImage;
    [self.trackingProgressView setImage:self.trackingImage];
    
    [self setNeedsLayout];
}

- (void)setBufferedImage:(UIImage *)bufferedImage
{
    _bufferedImage = bufferedImage;
    [self.bufferedProgressView setImage:self.bufferedImage];
    
    [self setNeedsLayout];
}

- (void)setToggleImage:(UIImage *)toggleImage
{
    _toggleImage = toggleImage;
    [self.toggleButton setFrame:CGRectMake(0, 0, self.toggleImage.size.width, self.toggleImage.size.height)];
    [self.toggleButton setImage:self.toggleImage forState:UIControlStateNormal];
    
    [self setNeedsLayout];
}

- (void)setValue:(CGFloat)value
{
    _value = MAX(MIN(value, 1.0), 0.0);
    
    [self setNeedsLayout];
}

- (void)setBufferedFrom:(CGFloat)bufferedFrom
{
    _bufferedFrom = MAX(MIN(bufferedFrom, 1.0), 0.0);
    
    [self setNeedsLayout];
}

- (void)setBufferedTo:(CGFloat)bufferedTo
{
    _bufferedTo = MAX(MIN(bufferedTo, 1.0), 0.0);
    
    [self setNeedsLayout];
}

@end