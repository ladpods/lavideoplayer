//
//  VPVideoPlayerSimpleControls.m
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 18/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerSimpleControls.h"
#import "LAVideoPlayerControlsProtocol.h"
#import "LAVideoPlayerCustom.h"
#import "LASlider.h"
#import "LADUtils.h"
#import "UIView+Utilities.h"
#import "NSBundle+PodBundle.h"

//----------------------------------------------

@interface LAVideoPlayerSimpleControls () <LAVideoPlayerControlsProtocol>

@property (nonatomic, strong) UIButton            *topBar;
@property (nonatomic, strong) UIButton            *doneButton;
@property (nonatomic, strong) UIButton            *playButton;
@property (nonatomic, strong) UIButton            *pauseButton;
@property (nonatomic, strong) UIButton            *chromecastButton;

@property (nonatomic, weak  ) LAVideoPlayerCustom *videoPlayer;

@property (nonatomic, assign) CGRect              initialRect;
@property (nonatomic, assign) CGFloat             itemDuration;
@property (nonatomic, assign) BOOL                isPlayingBeforeDragging;
@property (nonatomic, assign) BOOL                controlsDisplayed;

@end

// -------------------------------------------------

@implementation LAVideoPlayerSimpleControls

- (id)initWithPlayer:(LAVideoPlayerCustom *)videoPlayer
{
    self = [super init];
    
    if (self)
    {
        self.offset = 10;
        self.videoPlayer = videoPlayer;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleWirelessRoutesDidChange:)
                                                     name:MPVolumeViewWirelessRoutesAvailableDidChangeNotification object:nil];
    }
    
    return self;
}

- (void)startAnimatingChromecastButton
{
    self.chromecastButton.imageView.animationImages =
    @[ [NSBundle imageNamed:@"IconCastOn0"], [NSBundle imageNamed:@"IconCastOn1"],
       [NSBundle imageNamed:@"IconCastOn2"], [NSBundle imageNamed:@"IconCastOn1"] ];
     self.chromecastButton.imageView.animationDuration = 2;
    [self.chromecastButton.imageView startAnimating];
}

- (void)stopAnimatingChromecastButton
{
    [self.chromecastButton.imageView stopAnimating];
    self.chromecastButton.imageView.animationImages = nil;
    [self chromecastStatusUpdated];
}

- (void)setupDisplay
{
    [self setupTopBar];
    [self setupBottomBar];
    [self setupCenterDisplay];
    [self refreshDisplay];
    
    [self hideControls:NO];
}

- (void)refreshDisplay
{
    // Top Bar
    [self.topBar setFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
    
    [self.doneButton setFrame:CGRectMake(10, (self.topBar.frame.size.height - self.doneButton.frame.size.height) / 2, self.doneButton.frame.size.width, self.doneButton.frame.size.height)];
    
    [self.chromecastButton setFrame:CGRectMake(self.frame.size.width - self.chromecastButton.frame.size.width - self.offset, (self.topBar.frame.size.height - self.chromecastButton.frame.size.height) / 2, self.chromecastButton.frame.size.width, self.chromecastButton.frame.size.height)];
    
    // Bottom Bar
    [self.bottomBar setFrame:CGRectMake(0, self.frame.size.height - 44, self.frame.size.width, 44)];
    
    [self.progressLabel setFrame:CGRectMake(self.offset, 0, 40, self.bottomBar.frame.size.height)];
    
    [self.zoomInButton setFrame:CGRectMake(self.frame.size.width - self.zoomInButton.frame.size.width - self.offset, (self.bottomBar.frame.size.height - self.zoomInButton.frame.size.height) / 2, self.zoomInButton.frame.size.width, self.zoomInButton.frame.size.height)];
    [self.zoomOutButton setFrame:self.zoomInButton.frame];
    
    [self.volumeView setFrame:CGRectMake(self.zoomInButton.frame.origin.x - self.volumeView.frame.size.width - self.offset, (self.bottomBar.frame.size.height - self.volumeView.frame.size.height) / 2, self.volumeView.frame.size.width, self.volumeView.frame.size.height)];
    
    if(self.airPlayIsActive)
    {
        [self.durationLabel setFrame:CGRectMake(self.volumeView.frame.origin.x - self.volumeView.frame.size.width /*- self.offset*/, 0, 40, self.bottomBar.frame.size.height)];
    }
    else
    {
         [self.durationLabel setFrame:CGRectMake(self.frame.size.width - self.durationLabel.frame.size.width - self.offset, 0, 40, self.bottomBar.frame.size.height)];
    }
    
    [self.progressSlider setFrame:CGRectMake(self.progressLabel.frame.origin.x + self.progressLabel.frame.size.width + self.offset, (self.bottomBar.frame.size.height - self.progressSlider.frame.size.height) / 2, self.durationLabel.frame.origin.x - self.progressLabel.frame.origin.x - self.progressLabel.frame.size.width - 2 * self.offset, self.progressSlider.frame.size.height)];
    
    // Center
    [self.playButton setFrame:CGRectMake((self.frame.size.width - self.playButton.frame.size.width) / 2, (self.frame.size.height - self.playButton.frame.size.height) / 2, self.playButton.frame.size.width, self.playButton.frame.size.height)];
    
    [self.pauseButton setFrame:CGRectMake((self.frame.size.width - self.pauseButton.frame.size.width) / 2, (self.frame.size.height - self.pauseButton.frame.size.height) / 2, self.pauseButton.frame.size.width, self.pauseButton.frame.size.height)];
}

- (void)setupTopBar
{
    self.topBar = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.topBar setBackgroundImage:[NSBundle imageNamed:@"TopShadow"] forState:UIControlStateNormal];
    [self addSubview:self.topBar];
    
    UIImage *closeImage = [NSBundle imageNamed:@"IconClose"];
    self.doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.doneButton.alpha = 0;
    [self.doneButton setBackgroundImage:closeImage forState:UIControlStateNormal];
    [self.doneButton setFrame:CGRectMake(0, 0, closeImage.size.width, closeImage.size.height)];
    [self.doneButton addTarget:self action:@selector(handleClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:self.doneButton];
    
    UIImage *chromeCastButtonOff = [NSBundle imageNamed:@"BtnCastOff"];
    self.chromecastButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.chromecastButton addTarget:self action:@selector(handleChooseDevice:) forControlEvents:UIControlEventTouchUpInside];
    self.chromecastButton.frame = CGRectMake(0, 0, chromeCastButtonOff.size.width, chromeCastButtonOff.size.height);
    [self.chromecastButton setImage:nil forState:UIControlStateNormal];
    self.chromecastButton.alpha = 0;
    [self.topBar addSubview:self.chromecastButton];
}

- (void)setupBottomBar
{
    self.bottomBar = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar setAdjustsImageWhenHighlighted:NO];
    [self.bottomBar setBackgroundImage:[NSBundle imageNamed:@"BottomShadow"] forState:UIControlStateNormal];
    [self addSubview:self.bottomBar];
    
    self.progressLabel                 = [[UILabel alloc] initWithFrame:CGRectZero];
    self.progressLabel.backgroundColor = [UIColor clearColor];
    self.progressLabel.textColor       = [UIColor whiteColor];
    self.progressLabel.shadowColor     = [UIColor blackColor];
    self.progressLabel.shadowOffset    = CGSizeMake(0, 1);
    self.progressLabel.font            = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    self.progressLabel.textAlignment   = NSTextAlignmentCenter;
    [self.bottomBar addSubview:self.progressLabel];

    self.volumeView = [[MPVolumeView alloc] init];
    [self.volumeView setShowsVolumeSlider:NO];
    [self.volumeView sizeToFit];
    [self.bottomBar addSubview:self.volumeView];
    
    self.durationLabel                 = [[UILabel alloc] initWithFrame:CGRectZero];
    self.durationLabel.backgroundColor = [UIColor clearColor];
    self.durationLabel.textColor       = [UIColor whiteColor];
    self.durationLabel.shadowColor     = [UIColor blackColor];
    self.durationLabel.shadowOffset    = CGSizeMake(0, 1);
    self.durationLabel.font            = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    self.durationLabel.textAlignment   = NSTextAlignmentCenter;
    [self.bottomBar addSubview:self.durationLabel];
    
    self.progressSlider = [[LASlider alloc] initWithFrame:CGRectMake(self.progressLabel.frame.origin.x + self.progressLabel.frame.size.width + self.offset, self.pauseButton.frame.origin.y - 10, 213, 11)];
    [self.progressSlider setBackgroundImage:[[NSBundle imageNamed:@"BackgroundSlider"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
    [self.progressSlider setBufferedImage:[[NSBundle imageNamed:@"BufferingSlider"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
    [self.progressSlider setTrackingImage:[[NSBundle imageNamed:@"TrackingSlider"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
    [self.progressSlider setToggleImage:[NSBundle imageNamed:@"BtnToggle"]];
    [self.progressSlider addTarget:self action:@selector(handleSlider:) forControlEvents:UIControlEventValueChanged];
    [self.progressSlider addTarget:self action:@selector(handleSliderStartDragging:) forControlEvents:UIControlEventTouchDragEnter];
    [self.progressSlider addTarget:self action:@selector(handleSliderEndDragging:) forControlEvents:UIControlEventTouchDragExit];
    [self.bottomBar addSubview:self.progressSlider];
    
    UIImage *zoomInImage = [NSBundle imageNamed:@"BtnZoomIn"];
    self.zoomInButton    = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zoomInButton setFrame:CGRectMake(0, 0, zoomInImage.size.width, zoomInImage.size.height)];
    [self.zoomInButton setBackgroundImage:zoomInImage forState:UIControlStateNormal];
    [self.zoomInButton addTarget:self action:@selector(handleZoomIn) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomBar addSubview:self.zoomInButton];
    
    UIImage *zoomOutImage = [NSBundle imageNamed:@"BtnZoomOut"];
    self.zoomOutButton    = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zoomOutButton setFrame:CGRectMake(0, 0, zoomOutImage.size.width, zoomOutImage.size.height)];
    [self.zoomOutButton setBackgroundImage:zoomOutImage forState:UIControlStateNormal];
    [self.zoomOutButton addTarget:self action:@selector(handleZoomOut) forControlEvents:UIControlEventTouchUpInside];
    [self.zoomOutButton setAlpha:0.0];
    [self.bottomBar addSubview:self.zoomOutButton];
}

- (void)setupCenterDisplay
{
    UIImage *playImage = [NSBundle imageNamed:@"BtnPlay"];
    self.playButton    = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.playButton setBackgroundImage:playImage forState:UIControlStateNormal];
    [self.playButton setFrame:CGRectMake(0, 0, playImage.size.width, playImage.size.height)];
    [self.playButton addTarget:self action:@selector(handlePlay:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playButton];
    
    UIImage *pauseImage = [NSBundle imageNamed:@"BtnPause"];
    self.pauseButton    = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.pauseButton setFrame:CGRectMake(0, 0, pauseImage.size.width, pauseImage.size.height)];
    self.pauseButton.alpha = 0;
    [self.pauseButton setBackgroundImage:pauseImage forState:UIControlStateNormal];
    [self.pauseButton addTarget:self action:@selector(handlePause:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.pauseButton];
}

#pragma mark - Custom Images

- (void)setTopBarImage:(UIImage *)image
{
    [self.topBar setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setIconCloseImage:(UIImage *)image
{
    [self.doneButton setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
}

- (void)setBottomBarImage:(UIImage *)image
{
    [self.bottomBar setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
}

- (void)setBackgroundSliderImage:(UIImage *)image
{
    [self.progressSlider setBackgroundImage:[[UIImage imageNamed:image] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
}

- (void)setBufferingSliderImage:(UIImage *)image
{
    [self.progressSlider setBufferedImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
}

- (void)setTrackingSliderImage:(UIImage *)image
{
    [self.progressSlider setTrackingImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)]];
}

- (void)setBtnSliderImage:(UIImage *)image
{
    [self.progressSlider setToggleImage:image];
}

- (void)setBtnPauseImage:(UIImage *)image
{
    [self.pauseButton setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setBtnPlayImage:(UIImage *)image
{
    [self.playButton setBackgroundImage:image forState:UIControlStateNormal];
}

#pragma mark - Actions

- (void)handlePlay:(id)sender
{
    [self.videoPlayer resume];
}

- (void)handlePause:(id)sender
{
    if ([self.videoPlayer isPlaying])
    {
        [self.videoPlayer pause];
    }
}

- (void)handleZoomIn
{
    self.initialRect = self.videoPlayer.frame;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.videoPlayer refreshWithFrameWithAnimation:[LADUtils getScreenRect]];
                         self.zoomInButton.alpha = 0;
                         self.zoomOutButton.alpha = 1;
                         self.doneButton.alpha = 1;
                     }];
}

- (void)handleZoomOut
{
    if (self.initialRect.size.width == 0 || self.initialRect.size.height == 0)
    {
        return;
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.videoPlayer refreshWithFrameWithAnimation:self.initialRect];
                         self.zoomInButton.alpha = 1;
                         self.zoomOutButton.alpha = 0;
                         self.doneButton.alpha = 0;
                     }];
}

- (void)handleClose:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    UIViewController *viewController = [self firstAvailableUIViewController];
    
    if (viewController.presentingViewController)
    {
        [viewController dismissViewControllerAnimated:YES completion:^{}];
    }
    else
    {
        [self.videoPlayer refreshWithFrame:self.initialRect];
        self.zoomInButton.alpha = 1;
        self.zoomOutButton.alpha = 0;
        self.doneButton.alpha = 0;
    }
}

- (void)handleSlider:(id)sender
{
    BOOL autoResume = ([self.videoPlayer isPlaying] && !self.isPlayingBeforeDragging);
    
    if (autoResume)
    {
        [self.videoPlayer pause];
    }
    
    [self.videoPlayer seekToTime:CMTimeMake(self.progressSlider.value * [self.videoPlayer getItemDuration], 1)];
    
    if (autoResume)
    {
        [self.videoPlayer resume];
    }
}

- (void)handleSliderStartDragging:(id)sender
{
    if ([self.videoPlayer isPlaying])
    {
        self.isPlayingBeforeDragging = YES;
        [self.videoPlayer pause];
    }
}

- (void)handleSliderEndDragging:(id)sender
{
    if (self.isPlayingBeforeDragging)
    {
        [self.videoPlayer resume];
        self.isPlayingBeforeDragging = NO;
    }
}

- (void)handleChooseDevice:(id)sender
{
    [self.videoPlayer showDevicesSelectionForView:sender];
}

#pragma mark - LAVideoPlayerControls Protocol Implementation

- (void)showControls:(BOOL)animated
{
    self.controlsDisplayed = YES;
    
    // Dans tous les cas on affiche la barre supérieur pour pouvoir cacher le player si problème de chargement
    [UIView animateWithDuration:animated ? 0.15 : 0.0
                     animations:^{
                         self.topBar.alpha = 1;
                         [self.topBar setFrame:CGRectMake(0, 0, self.topBar.frame.size.width, self.topBar.frame.size.height)];
                         
                         if ([self.videoPlayer isPlaying])
                         {
                             self.pauseButton.alpha = 1.0;
                             self.playButton.alpha = 0.0;
                         }
                         else
                         {
                             self.playButton.alpha = 1.0;
                             self.pauseButton.alpha = 0.0;
                         }
                         
                         if (self.videoPlayer.isReadyForPlaying)
                         {
                             self.bottomBar.alpha = 1;
                             [self.bottomBar setFrame:CGRectMake(0, self.frame.size.height - self.bottomBar.frame.size.height, self.bottomBar.frame.size.width, self.bottomBar.frame.size.height)];
                         }
                     }];
}

- (void)hideControls:(BOOL)animated
{
    self.controlsDisplayed = NO;
    
    [UIView animateWithDuration:animated ? 0.1 : 0.0
                     animations:^{
                         self.topBar.alpha = 0;
                         self.bottomBar.alpha = 0;
                         self.pauseButton.alpha = 0.0;
                         self.playButton.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         [self.topBar setFrame:CGRectMake(0, -self.topBar.frame.size.height, self.topBar.frame.size.width, self.topBar.frame.size.height)];
                         [self.bottomBar setFrame:CGRectMake(0, self.frame.size.height, self.bottomBar.frame.size.width, self.bottomBar.frame.size.height)];
                     }];
}

- (void)videoDidPlay
{  
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.pauseButton.alpha = self.videoPlayer.isPlaying;
                         self.playButton.alpha = !self.videoPlayer.isPlaying;
                     }];
}

- (void)videoDidPause
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.pauseButton.alpha = 0;
                         self.playButton.alpha = 1;
                     }];
}

- (void)videoDidStop
{
    [self videoDidPause];
}

- (void)videoDidProgress:(CGFloat)secondsElapsed
{
    [self.progressLabel setText:[self stringFromTimeInterval:secondsElapsed]];
    [self.durationLabel setText:[self stringFromTimeInterval:self.itemDuration]];
    
    if(self.itemDuration > 0)
    {
        [self.progressSlider setValue:(secondsElapsed / self.itemDuration)];
    }
}

- (void)videoDidBufferedFrom:(CGFloat)startSeconds to:(CGFloat)toSeconds
{
    if(self.itemDuration > 0)
    {
        [self.progressSlider setBufferedFrom:(startSeconds / self.itemDuration)];
        [self.progressSlider setBufferedTo:(toSeconds / self.itemDuration)];
    }
}

- (void)videoMetadataLoaded:(CGFloat)duration
{
    self.itemDuration = duration;
}

- (void)videoDidExpand
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.zoomInButton.alpha  = 0;
                         self.zoomOutButton.alpha = 1;
                         self.doneButton.alpha    = 1;
                     }];
}

- (void)chromecastStatusUpdated
{
    if (self.videoPlayer.chromecastDevices.count == 0)
    {
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.chromecastButton.alpha = 0;
                             [self.chromecastButton setFrame:CGRectMake(self.frame.size.width - self.chromecastButton.frame.size.width - self.offset, (self.bottomBar.frame.size.height - self.chromecastButton.frame.size.height) / 2 + 10, self.chromecastButton.frame.size.width, self.chromecastButton.frame.size.height)];
                         }];
    }
    else
    {
        UIImage *chromeCastButtonOn  = [NSBundle imageNamed:@"BtnCastFilledOn"];
        UIImage *chromeCastButtonOff = [NSBundle imageNamed:@"BtnCastOff"];
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.chromecastButton.alpha = 1;
                             [self.chromecastButton setFrame:CGRectMake(self.frame.size.width - self.chromecastButton.frame.size.width - self.offset, (self.bottomBar.frame.size.height - self.chromecastButton.frame.size.height) / 2, self.chromecastButton.frame.size.width, self.chromecastButton.frame.size.height)];
                         }];
      
        [self.chromecastButton setImage:[self.videoPlayer isConnectedToChromecast] ? chromeCastButtonOn : chromeCastButtonOff forState:UIControlStateNormal];
    }
}

#pragma mark - Getters

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval
{
    NSInteger ti      = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

#pragma  mark - MPVolume delegate

- (void)handleWirelessRoutesDidChange:(NSNotification *)notification
{
    self.airPlayIsActive = YES;
    [self refreshDisplay];
}

- (void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self name:MPVolumeViewWirelessRoutesAvailableDidChangeNotification object:nil];
}

@end