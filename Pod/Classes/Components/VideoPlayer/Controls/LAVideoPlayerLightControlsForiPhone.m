//
//  LAVideoPlayerLightControlsForiPhone.m
//  LAVideoPlayerSample
//
//  Created by MIRAULT Fabien on 07/08/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerLightControlsForiPhone.h"
#import "LASlider.h"

@implementation LAVideoPlayerLightControlsForiPhone

- (void)setupDisplay
{
    [super setupDisplay];
    
    self.progressLabel.hidden = YES;
    self.durationLabel.hidden = YES;
}

- (void)refreshDisplay
{
    [super refreshDisplay];
    
    [self.progressSlider setFrame:CGRectMake(self.progressLabel.frame.origin.x, (self.bottomBar.frame.size.height - self.progressSlider.frame.size.height) / 2, self.durationLabel.frame.origin.x + self.durationLabel.frame.size.width - self.progressLabel.frame.origin.x, self.progressSlider.frame.size.height)];
}

@end