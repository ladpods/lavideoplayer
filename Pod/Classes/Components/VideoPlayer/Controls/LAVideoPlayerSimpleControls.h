//
//  VPVideoPlayerSimpleControls.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 18/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class LASlider;

@interface LAVideoPlayerSimpleControls : UIView

@property (nonatomic, assign) NSInteger    offset;

@property (nonatomic, strong) UIButton     *bottomBar;
@property (nonatomic, strong) UIButton     *zoomInButton;
@property (nonatomic, strong) UIButton     *zoomOutButton;
@property (nonatomic, strong) UILabel      *progressLabel;
@property (nonatomic, strong) UILabel      *durationLabel;
@property (nonatomic, strong) LASlider     *progressSlider;
@property (nonatomic, strong) MPVolumeView *volumeView;
@property (nonatomic, assign) BOOL airPlayIsActive;

// -------------------------------------------------

- (void)setupDisplay;
- (void)refreshDisplay;
- (void)handleZoomOut;

// -------------------------------------------------
- (void)setTopBarImage:(UIImage *)image;
- (void)setIconCloseImage:(UIImage *)image;
- (void)setBottomBarImage:(UIImage *)image;
- (void)setBackgroundSliderImage:(UIImage *)image;
- (void)setBufferingSliderImage:(UIImage *)image;
- (void)setTrackingSliderImage:(UIImage *)image;
- (void)setBtnSliderImage:(UIImage *)image;
- (void)setBtnPauseImage:(UIImage *)image;
- (void)setBtnPlayImage:(UIImage *)image;
@end