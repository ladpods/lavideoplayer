//
//  LAVideoPlayerFullScreenControls.m
//  LAVideoPlayerSample
//
//  Created by MIRAULT Fabien on 06/08/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerFullScreenControls.h"
#import "LASlider.h"

@implementation LAVideoPlayerFullScreenControls

- (void)setupDisplay
{
    [super setupDisplay];
    
    self.zoomInButton.hidden = YES;
    self.zoomOutButton.hidden = YES;
}

- (void)refreshDisplay
{
    [super refreshDisplay];
    
    // Bottom Bar
    [self.volumeView setFrame:CGRectMake(self.frame.size.width - self.volumeView.frame.size.width - self.offset, (self.bottomBar.frame.size.height - self.volumeView.frame.size.height) / 2, self.volumeView.frame.size.width, self.volumeView.frame.size.height)];
    
    if(self.airPlayIsActive)
    {
        [self.durationLabel setFrame:CGRectMake(self.volumeView.frame.origin.x - self.volumeView.frame.size.width /*- self.offset*/, 0, 40, self.bottomBar.frame.size.height)];
    }
    else
    {
        [self.durationLabel setFrame:CGRectMake(self.frame.size.width - self.durationLabel.frame.size.width - self.offset, 0, 40, self.bottomBar.frame.size.height)];
    }
    
    [self.progressSlider setFrame:CGRectMake(self.progressLabel.frame.origin.x + self.progressLabel.frame.size.width + self.offset, (self.bottomBar.frame.size.height - self.progressSlider.frame.size.height) / 2, self.durationLabel.frame.origin.x - self.progressLabel.frame.origin.x - self.progressLabel.frame.size.width - 2 * self.offset, self.progressSlider.frame.size.height)];
}

@end