//
//  LASlider.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 24/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerConstant.h"

@interface LASlider : UIControl

@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *trackingImage;
@property (nonatomic, strong) UIImage *bufferedImage;
@property (nonatomic, strong) UIImage *toggleImage;

@property (nonatomic, assign) CGFloat value;
@property (nonatomic, assign) CGFloat bufferedFrom;
@property (nonatomic, assign) CGFloat bufferedTo;

@end