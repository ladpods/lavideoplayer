//
//  VPVideoPlayer.m
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 17/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerCustom.h"
#import "LADUtils.h"
#import "UIView+Utilities.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

// ChromecastIMAAVPlayerContentPlayhead.h
#import <GoogleCast/GoogleCast.h>

// IMA SDK
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>
#import "NSBundle+PodBundle.h"

//----------------------------------------------

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

//----------------------------------------------

const char* AdEventNames[] = {
    "All Ads Complete",
    "Clicked",
    "Complete",
    "First Quartile",
    "Loaded",
    "Midpoint",
    "Pause",
    "Resume",
    "Third Quartile",
    "Started",
};

// -------------------------------------------------

@interface LAVideoPlayerCustom () <UIActionSheetDelegate, IMAAdsLoaderDelegate, IMAAdsManagerDelegate, IMAWebOpenerDelegate, GCKDeviceScannerListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate>

@property (nonatomic, assign) BOOL                          forcePause;
@property (nonatomic, assign) BOOL                          isAdPlayback;
@property (nonatomic, assign) BOOL                          isReadyForPlaying;
@property (nonatomic, assign) BOOL                          isControlsDisplayed;
@property (nonatomic, assign) BOOL                          chromecastAllowed;
@property (nonatomic, assign) BOOL                          videoComplete;
@property (nonatomic, assign) NSTimeInterval                lastProgression;
@property (nonatomic, assign) id                            playbackObserver;

// Chromecast
@property (nonatomic, strong) GCKMediaControlChannel        *mediaControlChannel;
@property (nonatomic, strong) GCKApplicationMetadata        *applicationMetadata;
@property (nonatomic, strong) GCKDeviceScanner              *deviceScanner;
@property (nonatomic, strong) GCKDeviceManager              *deviceManager;
@property (nonatomic, strong) GCKDevice                     *selectedDevice;

@property (nonatomic, strong) IMAAdsRenderingSettings       *adsRenderingSettings;
@property (nonatomic, strong) UIColor                       *originalBackgroundColor;
@property (nonatomic, strong) NSString                      *videoUrl;
@property (nonatomic, strong) NSString                      *adsTag;
@property (nonatomic, strong) NSString                      *gaAction;
@property (nonatomic, strong) NSString                      *gaLabel;
@property (nonatomic, strong) NSDictionary                  *gaDimensions;
@property (nonatomic, strong) UIView                        *videoView;
@property (nonatomic, strong) NSTimer                       *idleTimer;
@property (nonatomic, strong) NSTimer                       *castTimer;
@property (nonatomic, strong) AVQueuePlayer                 *videoPlayer;
@property (nonatomic, strong) AVPlayerLayer                 *videoPlayerLayer;
@property (nonatomic, strong) UILabel                       *externalDeviceDescription;
@property (nonatomic, strong) UIImageView                   *tvConnectedView;
@property (nonatomic, strong) IMAAdsLoader                  *adsLoader;
@property (nonatomic, strong) IMAAdsManager                 *adsManager;
@property (nonatomic, strong) IMAAdDisplayContainer         *adsDisplayContainer;
@property (nonatomic, strong) UIActivityIndicatorView       *activityindicatorView;
@property (nonatomic, strong) UIView<LAVideoPlayerControlsProtocol> *controls;
@property (nonatomic, strong) IMAAVPlayerContentPlayhead    *contentPlayhead;

@end

// -------------------------------------------------

@implementation LAVideoPlayerCustom

static const float kMaxIdleTime = 3.0;

#pragma mark - LifeCycle

- (void)dealloc
{
    [self notificationWillTerminate];
}

- (id)init
{
    self.clipsToBounds = YES;
    
    self = [super initWithFrame:[LADUtils getScreenRect]];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    self.backgroundColor = [UIColor blackColor];
    
    self.videoView = [[UIView alloc] initWithFrame:self.bounds];
    self.videoView.hidden = YES;
    
    [self setupAdsLoader];
    
    self.isAdPlayback = NO;
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
    
    //Chromecast device scanner
    self.deviceScanner = [[GCKDeviceScanner alloc] init];
    [self.deviceScanner addListener:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWillTerminate) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect screenRect = [LADUtils getScreenRect];
    UIViewController *viewController = [self firstAvailableUIViewController];
    
    if ((screenRect.size.width == self.frame.size.width && screenRect.size.height == self.frame.size.height) || (screenRect.size.width == self.frame.size.height && screenRect.size.height == self.frame.size.width))
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        if (!viewController.navigationController.navigationBarHidden)
        {
            [viewController.navigationController setNavigationBarHidden:YES animated:YES];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidExpand)])
        {
            [self.delegate videoDidExpand];
        }
        
        if ([self.controls respondsToSelector:@selector(videoDidExpand)])
        {
            [self.controls videoDidExpand];
        }
    }
    else
    {
        self.autoresizingMask = UIViewAutoresizingNone;
        
        if (viewController.navigationController.navigationBarHidden)
        {
            [viewController.navigationController setNavigationBarHidden:NO animated:YES];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidContract)])
        {
            [self.delegate videoDidContract];
        }
        
        if ([self.controls respondsToSelector:@selector(videoDidContract)])
        {
            [self.controls videoDidContract];
        }
    }
    
    if (self.frame.size.width > screenRect.size.width || self.frame.size.height > screenRect.size.height)
    {
        [self setFrame:screenRect];
    }
    
    if (self.videoPlayerLayer)
    {
        [self.videoPlayerLayer setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    
    if (self.controls)
    {
        [self.controls setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self.controls refreshDisplay];
    }
    
    [self.videoView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    if (self.tvConnectedView)
    {
        [self.tvConnectedView setFrame:CGRectMake((self.frame.size.width - self.tvConnectedView.frame.size.width) / 2, (self.frame.size.height - self.tvConnectedView.frame.size.height) / 2 - 20, self.tvConnectedView.frame.size.width, self.tvConnectedView.frame.size.height)];
        [self.externalDeviceDescription setFrame:CGRectMake(0, self.tvConnectedView.frame.origin.y + self.tvConnectedView.frame.size.height + 10, self.frame.size.width, 40)];
        
        if ([self isConnectedToChromecast] || [self isAirPlayActive])
        {
            [self.activityindicatorView setFrame:CGRectMake((self.frame.size.width - self.activityindicatorView.frame.size.width) / 2, self.tvConnectedView.frame.origin.y + self.tvConnectedView.frame.size.height / 2 - self.activityindicatorView.frame.size.height / 2, self.activityindicatorView.frame.size.width, self.activityindicatorView.frame.size.width)];
        }
        else
        {
            [self.activityindicatorView setFrame:CGRectMake((self.frame.size.width - self.activityindicatorView.frame.size.width) / 2, (self.frame.size.height - self.activityindicatorView.frame.size.height) / 2, self.activityindicatorView.frame.size.width, self.activityindicatorView.frame.size.width)];
        }
    }
    else
    {
        [self.activityindicatorView setFrame:CGRectMake((self.frame.size.width - self.activityindicatorView.frame.size.width) / 2, (self.frame.size.height - self.activityindicatorView.frame.size.height) / 2, self.activityindicatorView.frame.size.width, self.activityindicatorView.frame.size.width)];
    }
}

#pragma mark - Actions

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    if (self.isAdPlayback)
    {
        return;
    }
    
    [self stopIdleTimer];
    
    if (self.isControlsDisplayed)
    {
        [self hideControls];
    }
    else
    {
        [self showControls];
    }
}

#pragma mark - Timer

- (void)restartIdleTimer
{
    [self stopIdleTimer];
    [self startIdleTimer];
}

- (void)startIdleTimer
{
    if (self.autoHideControls)
    {
        [self stopIdleTimer];
        self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxIdleTime target:self selector:@selector(handleIdleTimer) userInfo:nil repeats:NO];
    }
}

- (void)stopIdleTimer
{
    if (self.idleTimer && [self.idleTimer isValid])
    {
        [self.idleTimer invalidate];
        self.idleTimer = nil;
    }
}

- (void)handleIdleTimer
{
    self.idleTimer = nil;
    [self hideControls];
}

- (void)restartCastTimer
{
    [self stopCastTimer];
    [self startCastTimer];
}

- (void)startCastTimer
{
    self.castTimer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(handleCastTimer) userInfo:nil repeats:YES];
}

- (void)stopCastTimer
{
    if (self.castTimer && [self.castTimer isValid])
    {
        [self.castTimer invalidate];
        self.castTimer = nil;
    }
}

- (void)handleCastTimer
{
    if ([self.controls respondsToSelector:@selector(videoDidProgress:)])
    {
        [self.controls videoDidProgress:self.mediaControlChannel.approximateStreamPosition];
    }
    
    // Si on tente de jouer la vidéo sur 2 devices différents
    if (self.videoPlayer.externalPlaybackActive && [self isConnectedToChromecast])
    {
        [self deviceDisconnected];
    }
}


#pragma mark - Private Display Functions

- (void)hideControls
{
    if ([self.controls respondsToSelector:@selector(hideControls:)])
    {
        [self.controls hideControls:YES];
    }
    
    self.isControlsDisplayed = NO;
}

- (void)showControls
{
    if ([self.controls respondsToSelector:@selector(showControls:)])
    {
        [self.controls showControls:YES];
        [self startIdleTimer];
    }
    
    self.isControlsDisplayed = YES;
}

#pragma mark - Private Functions

- (void)playVideo
{
    if ([self isConnectedToChromecast])
    {
        [self startCastTimer];
        [self.mediaControlChannel play];
    }
    else
    {
        [self.videoPlayer play];
    }
    
    if ([self.controls respondsToSelector:@selector(videoDidPlay)])
    {
        [self.controls videoDidPlay];
        [self startIdleTimer];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidStart)])
    {
        [self.delegate videoDidStart];
    }
}

- (void)pauseVideo
{
    if ([self isConnectedToChromecast])
    {
        [self stopCastTimer];
        [self.mediaControlChannel pause];
    }
    else
    {
        [self.videoPlayer pause];
    }
    
    if ([self.controls respondsToSelector:@selector(videoDidPause)])
    {
        [self.controls videoDidPause];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidPause)])
    {
        [self.delegate videoDidPause];
    }
}

- (void)stopVideo
{
    if ([self isConnectedToChromecast])
    {
        
        [self stopCastTimer];
        [self.mediaControlChannel stop];
    }
    else
    {
        [self.videoPlayer pause];
    }
    
    if ([self.controls respondsToSelector:@selector(videoDidStop)])
    {
        [self.controls videoDidStop];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidStop)])
    {
        [self.delegate videoDidStop];
    }
}

#pragma mark - Public Functions

- (void)refreshWithFrame:(CGRect)newFrame
{
    CGRect screenRect = [LADUtils getScreenRect];
    if ((screenRect.size.width == newFrame.size.width && screenRect.size.height == newFrame.size.height) || (screenRect.size.width == newFrame.size.height && screenRect.size.height == newFrame.size.width))
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoWillExpand)])
        {
            [self.delegate videoWillExpand];
        }
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoWillContract)])
        {
            [self.delegate videoWillContract];
        }
    }
    
    [self setFrame:newFrame];
    
    if (self.videoPlayerLayer)
    {
        [CATransaction begin];
        [CATransaction setAnimationDuration:0];
        [CATransaction setDisableActions:YES];
        [self.videoPlayerLayer setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
        [CATransaction commit];
    }
    
    if (self.controls)
    {
        [self.controls setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
        [self.controls refreshDisplay];
    }
    
    [self.videoView setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
    
    [self.activityindicatorView setFrame:CGRectMake((newFrame.size.width - self.activityindicatorView.frame.size.width) / 2, (newFrame.size.height - self.activityindicatorView.frame.size.height) / 2, self.activityindicatorView.frame.size.width, self.activityindicatorView.frame.size.width)];
}

- (void)refreshWithFrameWithAnimation:(CGRect)newFrame
{
    CGRect screenRect = [LADUtils getScreenRect];
    if ((screenRect.size.width == newFrame.size.width && screenRect.size.height == newFrame.size.height) || (screenRect.size.width == newFrame.size.height && screenRect.size.height == newFrame.size.width))
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoWillExpand)])
        {
            [self.delegate videoWillExpand];
        }
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(videoWillContract)])
        {
            [self.delegate videoWillContract];
        }
    }
    
    [self setFrame:newFrame];
    
    if (self.videoPlayerLayer)
    {
        [self.videoPlayerLayer setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
    }
    
    if (self.controls)
    {
        [self.controls setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
        [self.controls refreshDisplay];
    }
    
    [self.videoView setFrame:CGRectMake(0, 0, newFrame.size.width, newFrame.size.height)];
    
    [self.activityindicatorView setFrame:CGRectMake((newFrame.size.width - self.activityindicatorView.frame.size.width) / 2, (newFrame.size.height - self.activityindicatorView.frame.size.height) / 2, self.activityindicatorView.frame.size.width, self.activityindicatorView.frame.size.width)];
}

- (void)playVideoWithUrl:(NSString *)videoUrl andAdsTag:(NSString *)adsTag
            andGaAction:(NSString *)gaAction andGaLabel:(NSString *)gaLabel andGaDimensions:(NSDictionary *)gaDimensions
{
    self.adsTag            = adsTag;
    self.videoUrl          = videoUrl;
    self.isReadyForPlaying = NO;
    
    // Google Analytics parameters | check they aren't nil
    if (gaAction != nil)
    {
        self.gaAction = gaAction;
    }
    else
    {
        self.gaAction = @"";
    }
    
    if (gaLabel != nil)
    {
        self.gaLabel=gaLabel;
    }
    else
    {
        self.gaLabel = @"";
    }
    
    if (gaDimensions != nil)
    {
        self.gaDimensions = gaDimensions;
    }
    else
    {
        self.gaDimensions = [[NSDictionary alloc]initWithObjectsAndKeys:@"",@"",nil];
    }
    
    if (self.videoPlayer)
    {
        [self.videoPlayerLayer removeFromSuperlayer];
        self.videoPlayerLayer = nil;
        self.videoPlayer = nil;
    }
    
    if (!self.activityindicatorView)
    {
        self.activityindicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self.activityindicatorView startAnimating];
    }
    
    NSString *fileExtension = [self.videoUrl pathExtension];
    /*NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
     NSString *contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);*/
    self.chromecastAllowed = ![[fileExtension lowercaseString] isEqualToString:@"m3u8"];
    
    if (!self.chromecastAllowed)
    {
        [self.deviceScanner stopScan];
        [self deviceStatusUpdated];
    }
    else
    {
        [self.deviceScanner startScan];
        
        if (!self.tvConnectedView)
        {
            self.tvConnectedView       = [[UIImageView alloc] initWithImage:[NSBundle imageNamed:@"TVConnected"]];
            self.tvConnectedView.alpha = 0;
            
            self.externalDeviceDescription                 = [[UILabel alloc] init];
            self.externalDeviceDescription.textColor       = [UIColor whiteColor];
            self.externalDeviceDescription.textAlignment   = NSTextAlignmentCenter;
            self.externalDeviceDescription.backgroundColor = [UIColor clearColor];
            self.externalDeviceDescription.alpha           = 0;
        }
    }
    
    AVURLAsset *contentAsset        = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:videoUrl] options:0];
    AVPlayerItem *contentPlayerItem = [AVPlayerItem playerItemWithAsset:contentAsset];
    self.videoPlayer = [AVQueuePlayer playerWithPlayerItem:contentPlayerItem];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0") && [self.controls respondsToSelector:@selector(videoMetadataLoaded:)])
    {
        [self.controls videoMetadataLoaded:[self getItemDuration]];
    }
    
    __weak LAVideoPlayerCustom *weakSelf = self;
    CMTime interval = CMTimeMake(33, 1000);  // 30fps
    self.playbackObserver = [self.videoPlayer addPeriodicTimeObserverForInterval:interval queue:nil usingBlock:^(CMTime time) {
        
        NSTimeInterval timeInSeconds = CMTimeGetSeconds(time);
        
        if (weakSelf.lastProgression != timeInSeconds && weakSelf.activityindicatorView.isAnimating && [weakSelf isPlaying])
        {
            [weakSelf.activityindicatorView stopAnimating];
        }
        
        // Si on tente de jouer la vidéo sur 2 devices différents
        if (weakSelf.videoPlayer.externalPlaybackActive && [weakSelf isConnectedToChromecast])
        {
            [weakSelf deviceDisconnected];
        }
        
        if (weakSelf.videoPlayer.externalPlaybackActive)
        {
            if (!weakSelf.originalBackgroundColor)
            {
                weakSelf.originalBackgroundColor = weakSelf.backgroundColor;
                weakSelf.backgroundColor = [UIColor colorWithRed:0.108 green:0.107 blue:0.107 alpha:1];
                weakSelf.externalDeviceDescription.text = LocalizedString(@"PLAYING_ON_AIRPLAY", nil);
            }
        }
        else if (weakSelf.originalBackgroundColor && ![weakSelf isConnectedToChromecast])
        {
            weakSelf.backgroundColor = weakSelf.originalBackgroundColor;
            weakSelf.originalBackgroundColor = nil;
        }
        
        if (![weakSelf isConnectedToChromecast])
        {
            weakSelf.tvConnectedView.alpha = weakSelf.videoPlayer.externalPlaybackActive;
            weakSelf.externalDeviceDescription.alpha = weakSelf.videoPlayer.externalPlaybackActive;
        }
        
        weakSelf.lastProgression = timeInSeconds;
        
        if ([weakSelf.controls respondsToSelector:@selector(videoDidProgress:)])
        {
            [weakSelf.controls videoDidProgress:timeInSeconds];
        }
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(videoDidProgress:)])
        {
            [weakSelf.delegate videoDidProgress:timeInSeconds];
        }
        
        if ([weakSelf.controls respondsToSelector:@selector(videoDidBufferedFrom:to:)])
        {
            NSArray *loadedTimeRanges = [[weakSelf.videoPlayer currentItem] loadedTimeRanges];
            
            if (loadedTimeRanges.count > 0)
            {
                CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
                float startSeconds = CMTimeGetSeconds(timeRange.start);
                float durationSeconds = CMTimeGetSeconds(timeRange.duration);
                NSTimeInterval result = startSeconds + durationSeconds;
                [weakSelf.controls videoDidBufferedFrom:startSeconds to:result];
            }
        }
    }];
    
    [self.videoPlayer addObserver:self forKeyPath:@"currentItem.duration" options:0 context:@"playerDuration"];
    [contentPlayerItem addObserver:self forKeyPath:@"status" options:0 context:nil];
    [contentPlayerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    [contentPlayerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    
    self.contentPlayhead = [[IMAAVPlayerContentPlayhead alloc] initWithAVPlayer:self.videoPlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationContentDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:contentPlayerItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    // Attach the content player to the Video view.
    self.videoPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.videoPlayer];
    self.videoPlayerLayer.frame = self.bounds;
    self.videoPlayerLayer.hidden = YES;
    [self.layer addSublayer:self.videoPlayerLayer];
    
    if (self.tvConnectedView)
    {
        [self.layer addSublayer:self.tvConnectedView.layer];
        [self.layer addSublayer:self.externalDeviceDescription.layer];
    }
    
    [self.layer addSublayer:self.activityindicatorView.layer];
    [self.layer addSublayer:self.controls.layer];
    
    // Create an adsRequest object and request ads from the ad server.
    [self unloadAdsManager];
    
    if (adsTag && ![self isAirPlayActive] && ![self isConnectedToChromecast])
    {
        // Create an adDisplayContainer with the ad container and companion ad slots.
        self.adsDisplayContainer = [[IMAAdDisplayContainer alloc] initWithAdContainer:self.videoView companionSlots:nil];
       
        IMAAdsRequest *request =  [[IMAAdsRequest alloc]  initWithAdTagUrl:adsTag adDisplayContainer:self.adsDisplayContainer contentPlayhead:self.contentPlayhead userContext:nil];
        //IMAAdsRequest *request = [[IMAAdsRequest alloc] initWithAdTagUrl:adsTag companionSlots:nil userContext:nil];
        [self.adsLoader requestAdsWithRequest:request];
    }
    else
    {
        [self resume];
        self.videoPlayerLayer.hidden = NO;
    }
}

- (BOOL)isAirPlayActive
{
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    
    BOOL airplayLocated = NO;
    
    for( AVAudioSessionPortDescription *portDescription in route.outputs )
    {
        airplayLocated |= ( [portDescription.portType isEqualToString:AVAudioSessionPortAirPlay] );
    }
    
    return airplayLocated;
}

- (void)resume
{
    if (!self.isAdPlayback)
    {
        [self playVideo];
        [self restartIdleTimer];
    }
    else
    {
        [self.adsManager resume];
    }
}

- (void)pause
{
    if (!self.isAdPlayback)
    {
        [self pauseVideo];
        [self restartIdleTimer];
    }
    else
    {
        [self.adsManager pause];
    }
}

- (void)seekToTime:(CMTime)time
{
    if ([self isConnectedToChromecast])
    {
        [self.mediaControlChannel seekToTimeInterval:CMTimeGetSeconds(time)];
    }
    else
    {
        if (!self.activityindicatorView.isAnimating)
        {
            [self.activityindicatorView startAnimating];
        }
        
        [self.videoPlayer seekToTime:time];
    }
    
    [self restartIdleTimer];
}

- (void)stop
{
    [self stopVideo];
    [self seekToTime:CMTimeMake(0, 1)];
    
    if ([self.controls respondsToSelector:@selector(videoDidProgress:)])
    {
        [self.controls videoDidProgress:0];
    }
}

- (void)clear
{
    [self unloadAdsManager];
    [self stop];
    
    @try
    {
        [self.videoPlayer removeObserver:self forKeyPath:@"currentItem.duration"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"status"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    }
    @catch (NSException *exception)
    {
    }
    
    [self.videoPlayer removeAllItems];
    [self stopIdleTimer];
    [self.deviceScanner removeListener:self];
    [self.deviceScanner stopScan];
    [self deviceStatusUpdated];
}

- (void)changeVolume:(CGFloat)volume
{
    NSArray *audioTracks = [self.videoPlayer.currentItem.asset tracksWithMediaType:AVMediaTypeAudio];
    NSMutableArray *allAudioParams = [NSMutableArray array];
    
    for (AVAssetTrack *track in audioTracks)
    {
        AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters];
        [audioInputParams setVolume:MIN(MAX(volume, 0.0), 1.0) atTime:kCMTimeZero];
        [audioInputParams setTrackID:[track trackID]];
        [allAudioParams addObject:audioInputParams];
    }
    
    AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
    [audioMix setInputParameters:allAudioParams];
    
    [self.videoPlayer.currentItem setAudioMix:audioMix];
}

- (void)showDevicesSelectionForView:(UIView *)originView
{
    if ([self isAirPlayActive])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocalizedString(@"ERROR", nil)
                                                        message:LocalizedString(@"ALREADY_ON_EXTERNAL_DEVICE", nil)
                                                       delegate:nil
                                              cancelButtonTitle:LocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (!self.selectedDevice)
    {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:LocalizedString(@"CONNECT_TO_DEVICE", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
        
        for (GCKDevice *device in self.deviceScanner.devices)
        {
            [sheet addButtonWithTitle:device.friendlyName];
        }
        
        [sheet addButtonWithTitle:LocalizedString(@"CANCEL", nil)];
        sheet.cancelButtonIndex = [self.deviceScanner.devices count];
        
        [sheet showInView:originView];
    }
    else
    {
        NSString *friendlyName = [NSString stringWithFormat:@"%@ %@", LocalizedString(@"CONNECTED_TO", nil), self.selectedDevice.friendlyName];
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:friendlyName delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
        
        [sheet addButtonWithTitle:LocalizedString(@"DISCONNECT", nil)];
        [sheet addButtonWithTitle:LocalizedString(@"CANCEL", nil)];
        sheet.destructiveButtonIndex = 0;
        sheet.cancelButtonIndex = 1;
        [sheet showInView:originView];
    }
}

#pragma mark - Notifications

- (void)notificationContentDidFinishPlaying:(NSNotification *)notification
{
    self.videoComplete = YES;
    
    [self clear];
    [self.adsLoader contentComplete];
    
    [self playVideoWithUrl:self.videoUrl andAdsTag:nil andGaAction:nil andGaLabel:nil andGaDimensions:nil]; // A la fin de la vidéo, on ne rejoue pas la publicité
    [self pause];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(videoDidComplete)])
    {
        [self.delegate videoDidComplete];
    }
}

- (void)notificationWillEnterForeground
{
    if (self.isAdPlayback)
    {
        [self.adsManager resume];
    }
}

- (void)notificationDidEnterBackground
{
    if (!self.isAdPlayback)
    {
        [self pause];
    }
}

- (void)notificationWillTerminate
{
    if ([self isConnectedToChromecast])
    {
        [self deviceDisconnected];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.videoPlayer removeTimeObserver:self.playbackObserver];
    @try {
        [self.videoPlayer removeObserver:self forKeyPath:@"currentItem.duration"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"status"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [self.videoPlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    }
    @catch (NSException *exception)
    {
        
    }
    
    self.delegate = nil;
    
    [self stopIdleTimer];
    [self resetAppState];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == @"playerDuration" && self.videoPlayer == object)
    {
        if ([self.controls respondsToSelector:@selector(videoMetadataLoaded:)])
        {
            [self.controls videoMetadataLoaded:[self getItemDuration]];
        }
    }
    else if ([keyPath isEqualToString:@"status"])
    {
        AVPlayerItem *item = (AVPlayerItem *)object;
        switch(item.status)
        {
            case AVPlayerItemStatusReadyToPlay:
                self.isReadyForPlaying = YES;
                if (self.isControlsDisplayed)
                {
                    [self showControls];
                }
                
            case AVPlayerItemStatusFailed:
            case AVPlayerItemStatusUnknown:
                //[self.activityindicatorView stopAnimating];
                break;
        }
        
        if (item.status == AVPlayerStatusFailed)
        {
            [self clear];
        }
    }
    else if ([keyPath isEqualToString:@"playbackBufferEmpty"])
    {
        if (self.videoPlayer.currentItem.playbackBufferEmpty)
        {
            [self.activityindicatorView startAnimating];
        }
    }
    
    else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"])
    {
        if (self.videoPlayer.currentItem.playbackLikelyToKeepUp)
        {
            //[self.activityindicatorView stopAnimating];
        }
    }
}

#pragma mark - Status Reset

- (void)resetAppState
{
    self.adsManager.delegate = nil;
    [self unloadAdsManager];
    [self.adsManager destroy];
    [self clear];
    self.isAdPlayback = NO;
}

#pragma mark - Chromecast

- (void)connectToDevice
{
    if (!self.selectedDevice)
    {
        return;
    }
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    self.deviceManager = [[GCKDeviceManager alloc] initWithDevice:self.selectedDevice clientPackageName:[info objectForKey:@"CFBundleIdentifier"]];
    self.deviceManager.delegate = self;
    
    [self.deviceManager connect];
    [self.controls startAnimatingChromecastButton];
    
    
}

- (void)deviceDisconnected
{
    NSTimeInterval approximateStreamPosition = CMTimeGetSeconds(self.videoPlayer.currentTime);
    if ([self isConnectedToChromecast])
    {
        approximateStreamPosition = self.mediaControlChannel.approximateStreamPosition;
    }
    
    [self.mediaControlChannel stop];
    self.mediaControlChannel = nil;
    
    self.deviceManager.delegate = nil;
    [self.deviceManager leaveApplication];
    [self.deviceManager disconnect];
    self.deviceManager = nil;
    
    self.selectedDevice = nil;
    
    [self deviceStatusUpdated];
    [self stopCastTimer];
    
    self.tvConnectedView.alpha = 0;
    self.externalDeviceDescription.alpha = 0;
    
    if (self.originalBackgroundColor)
    {
        self.backgroundColor = self.originalBackgroundColor;
        self.originalBackgroundColor = nil;
    }
    
    self.videoPlayerLayer.hidden = NO;
    [self seekToTime:CMTimeMake(approximateStreamPosition, 1)];
    [self resume];
}

- (void)deviceStatusUpdated
{
    if ([self.controls respondsToSelector:@selector(chromecastStatusUpdated)])
    {
        [self.controls chromecastStatusUpdated];
    }
}

- (void)castVideo
{
    if (![self isConnectedToChromecast])
    {
        return;
    }
    
    self.externalDeviceDescription.text = [NSString stringWithFormat:LocalizedString(@"PLAYING_ON_CHROMECAST", nil), self.selectedDevice.friendlyName];
    
    //Define Media metadata
    GCKMediaMetadata *metadata = [[GCKMediaMetadata alloc] init];
    //    GCKMediaInformation *mediaInformation = [[GCKMediaInformation alloc] initWithContentID:self.videoUrl streamType:GCKMediaStreamTypeNone contentType:@"application/x-mpegurl" metadata:metadata streamDuration:[self getItemDuration] customData:nil];
    
    GCKMediaInformation *mediaInformation = [[GCKMediaInformation alloc] initWithContentID:self.videoUrl streamType:GCKMediaStreamTypeBuffered contentType:nil metadata:metadata streamDuration:0 customData:nil];
    
    //Cast video
    self.forcePause = (self.videoPlayer.rate == 0);
    self.originalBackgroundColor = self.backgroundColor;
    self.backgroundColor = [UIColor colorWithRed:0.108 green:0.107 blue:0.107 alpha:1];
    
    [self.mediaControlChannel loadMedia:mediaInformation autoplay:YES playPosition:CMTimeGetSeconds(self.videoPlayer.currentTime)];
    [self.videoPlayer pause];
    self.videoPlayerLayer.hidden = YES;
    self.tvConnectedView.alpha = 1;
    self.externalDeviceDescription.alpha = 1;
    
    [self.activityindicatorView startAnimating];
}

#pragma mark - Ads Manager

- (void)setupAdsLoader
{
    IMASettings *settings = [self getIMASettings];
    self.adsLoader = [[IMAAdsLoader alloc] initWithSettings:settings];
    self.adsLoader.delegate = self;
}

- (void)unloadAdsManager
{
    if (self.adsManager)
    {
        self.adsRenderingSettings.webOpenerPresentingController = nil;
        
        [self.adsManager destroy];
        self.adsDisplayContainer = nil;
        self.adsManager.delegate = nil;
        self.adsManager = nil;
    }
}

#pragma mark - AdsLoader Delegate

- (void)adsLoader:(IMAAdsLoader *)loader adsLoadedWithData:(IMAAdsLoadedData *)adsLoadedData
{
    self.adsManager = adsLoadedData.adsManager;
    
    if (self.adsManager.adCuePoints.count > 0)
    {
        NSMutableString *cuePoints = [NSMutableString stringWithString:@"("];
        for (NSNumber *cuePoint in self.adsManager.adCuePoints)
        {
            [cuePoints appendFormat:@"%@, ", cuePoint];
        }
        
        [cuePoints replaceCharactersInRange:NSMakeRange([cuePoints length] - 2, 2) withString:@")"];
    }
    
    self.adsManager.delegate = self;
    self.videoView.frame = self.bounds;
    [self addSubview:self.videoView];
    
    self.adsRenderingSettings = [[IMAAdsRenderingSettings alloc] init];
    self.adsRenderingSettings.webOpenerDelegate = self;
    self.adsRenderingSettings.webOpenerPresentingController = [self firstAvailableUIViewController];
    self.adsRenderingSettings.bitrate = kIMAAutodetectBitrate;
    self.adsRenderingSettings.mimeTypes = @[];
    [self.adsManager initializeWithAdsRenderingSettings:self.adsRenderingSettings];
}

- (void)adsLoader:(IMAAdsLoader *)loader failedWithErrorData:(IMAAdLoadingErrorData *)adErrorData
{
    //NSLog(@"Ad loading error: code:%d, message: %@", adErrorData.adError.code, adErrorData.adError.message);
    [self resume];
    
    self.videoPlayerLayer.hidden = NO;
}

#pragma mark - IMABrowser Delegate implementation

- (void)browserDidOpen
{
    //NSLog(@"In-app browser did open.");
}

- (void)browserDidClose
{
    //NSLog(@"In-app browser did close.");
}

#pragma mark - AdsManager Delegate implementation

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdEvent:(IMAAdEvent *)event
{
    //NSLog(@"AdsManager event (%s).", AdEventNames[event.type]);
    
    switch (event.type)
    {
        case kIMAAdEvent_LOADED:
            [adsManager start];
            break;
            
        case kIMAAdEvent_ALL_ADS_COMPLETED:
            self.videoPlayerLayer.hidden = NO;
            [self unloadAdsManager];
            break;
            
        case kIMAAdEvent_STARTED:
        {
            [self hideControls];
            
            self.videoView.hidden = NO;
            self.videoComplete = NO;
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(adDidStart)])
            {
                [self.delegate adDidStart];
            }
            //NSLog(@"Showing ad %d/%d, bumper: %@, pod index: %d, time offset: %lf, max duration: %lf", event.ad.adPodInfo.adPosition, event.ad.adPodInfo.totalAds, event.ad.adPodInfo.isBumper ? @"YES" : @"NO", event.ad.adPodInfo.podIndex, event.ad.adPodInfo.timeOffset, event.ad.adPodInfo.maxDuration);
            break;
        }
            
        case kIMAAdEvent_COMPLETE:
            break;
            
        default:
            break;
    }
}

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdError:(IMAAdError *)error
{
#ifdef DEBUG
    NSLog(@" LAVideoPlayerCustom didReceiveAdError : AdsManager error with type: %ld\ncode: %ld\nmessage: %@", (long)error.type, (long)error.code, error.message);
#endif
    
    if (!self.videoComplete)
    {
        [self adsManagerDidRequestContentResume:adsManager];
    }
    else
    {
        self.videoComplete = NO;
    }
}

- (void)adsManagerDidRequestContentPause:(IMAAdsManager *)adsManager
{
    //NSLog(@"AdsManager requested content pause.");
    [self pauseVideo];
    self.isAdPlayback = YES;
    
    self.videoPlayerLayer.hidden = YES;
}

- (void)adsManagerDidRequestContentResume:(IMAAdsManager *)adsManager
{
    //NSLog(@"AdsManager requested content resume.");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(adDidComplete)])
    {
        [self.delegate adDidComplete];
    }
    
    self.videoView.hidden = YES;
    
    [self playVideo];
    self.isAdPlayback = NO;
    
    self.videoPlayerLayer.hidden = NO;
}

- (void)adDidProgressToTime:(NSTimeInterval)mediaTime totalTime:(NSTimeInterval)totalTime
{
    //    CMTime time = CMTimeMakeWithSeconds(mediaTime, 1000);
    //    CMTime duration = CMTimeMakeWithSeconds(totalTime, 1000);
    //    Float64 currentTime = CMTimeGetSeconds(time);
    //    Float64 durationTime = CMTimeGetSeconds(duration);
    
    //NSLog(@"Time : %@, duration : %@", [NSString stringWithFormat:@"%d:%02d", (int)currentTime / 60, (int)currentTime % 60], [NSString stringWithFormat:@"%d:%02d", (int)durationTime / 60, (int)durationTime % 60]);
    //[self updatePlayHeadWithTime:time duration:duration];
    //self.progressBar.maximumValue = totalTime;
}

#pragma mark - IMABrowser delegate functions

- (void)willOpenExternalBrowser
{
    //NSLog(@"External browser will open.");
}

- (void)willOpenInAppBrowser
{
    //NSLog(@"In-app browser will open");
    if (self.delegate && [self.delegate respondsToSelector:@selector(adWillOpenInAppBrowser)])
    {
        [self.delegate adWillOpenInAppBrowser];
    }
}

- (void)didOpenInAppBrowser
{
    //NSLog(@"In-app browser did open");
}

- (void)willCloseInAppBrowser
{
    //NSLog(@"In-app browser will close");
    if (self.delegate && [self.delegate respondsToSelector:@selector(adWillCloseInAppBrowser)])
    {
        [self.delegate adWillCloseInAppBrowser];
    }
}

- (void)didCloseInAppBrowser
{
    //NSLog(@"In-app browser did close");
}

#pragma mark - GCKDeviceScannerListener Delegate

- (void)deviceDidComeOnline:(GCKDevice *)device
{
    [self deviceStatusUpdated];
}

- (void)deviceDidGoOffline:(GCKDevice *)device
{
    [self deviceStatusUpdated];
}

#pragma mark - GCKDeviceManagerDelegate

- (void)deviceManagerDidConnect:(GCKDeviceManager *)deviceManager
{
    [self deviceStatusUpdated];
    [self.deviceManager launchApplication:kGCKMediaDefaultReceiverApplicationID];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didConnectToCastApplication:(GCKApplicationMetadata *)applicationMetadata sessionID:(NSString *)sessionID launchedApplication:(BOOL)launchedApplication
{
    self.mediaControlChannel = [[GCKMediaControlChannel alloc] init];
    self.mediaControlChannel.delegate = self;
    [self.deviceManager addChannel:self.mediaControlChannel];
    [self.mediaControlChannel requestStatus];
    [self castVideo];
    [self.controls stopAnimatingChromecastButton];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didFailToConnectToApplicationWithError:(NSError *)error
{
    [self showError:error];
    [self deviceDisconnected];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didFailToConnectWithError:(GCKError *)error
{
    [self showError:error];
    [self deviceDisconnected];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectWithError:(GCKError *)error
{
    if (error)
    {
        [self showError:error];
    }
    
    [self deviceDisconnected];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didReceiveStatusForApplication:(GCKApplicationMetadata *)applicationMetadata
{
    self.applicationMetadata = applicationMetadata;
}

- (void)showError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocalizedString(@"ERROR", nil)
                                                    message:LocalizedString(error.description, nil)
                                                   delegate:nil
                                          cancelButtonTitle:LocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - GCKMediaControlChannel Delegate

- (void)mediaControlChannelDidUpdateStatus:(GCKMediaControlChannel *)mediaControlChannel
{
    if (mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePlaying)
    {
        [self.activityindicatorView stopAnimating];
        
        if ([self.controls respondsToSelector:@selector(videoDidProgress:)])
        {
            [self.controls videoDidProgress:mediaControlChannel.approximateStreamPosition];
            [self restartCastTimer];
        }
        
        if (self.forcePause)
        {
            [self pauseVideo];
            self.forcePause = NO;
        }
    }
    else if (mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePaused)
    {
        
    }
    else if (mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStateBuffering)
    {
        [self.activityindicatorView startAnimating];
        [self stopCastTimer];
    }
}

- (void)mediaControlChannelDidUpdateMetadata:(GCKMediaControlChannel *)mediaControlChannel
{
    //NSLog(@"mediaControlChannelDidUpdateMetadata");
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.selectedDevice == nil)
    {
        if (buttonIndex < self.deviceScanner.devices.count)
        {
            self.selectedDevice = self.deviceScanner.devices[buttonIndex];
            [self connectToDevice];
        }
    }
    else
    {
        if (buttonIndex == 0)
        {
            [self deviceDisconnected];
        }
    }
}

#pragma mark - Getters

- (IMASettings *)getIMASettings
{
    IMASettings *settings = [[IMASettings alloc] init];
    settings.ppid = @"IMA_PPID_0";
    settings.language = @"fr";
    return settings;
}

- (BOOL)isPlaying
{
    return [self isConnectedToChromecast] ? self.mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePlaying : (self.videoPlayer.rate != 0);
}

- (CGFloat)getItemDuration
{
    return CMTimeGetSeconds(self.videoPlayer.currentItem.duration);
}

- (BOOL)isConnectedToChromecast
{
    return self.deviceManager && self.deviceManager.isConnected;
}

- (NSArray *)chromecastDevices
{
    return self.chromecastAllowed ? self.deviceScanner.devices : 0;
}

- (UIView<LAVideoPlayerControlsProtocol> *)getControls
{
    return self.controls;
}

#pragma mark - Setters

- (void)setControlsClass:(Class)controlsClass
{
    if (![controlsClass conformsToProtocol:@protocol(LAVideoPlayerControlsProtocol)])
    {
#ifdef DEBUG
        NSLog(@"- LAVideoPlayerCustom setControlsClass:() :Your controls must implement VPVideoPlayerControlsProtocol");
#endif
        return;
    }
    
    if (![controlsClass isSubclassOfClass:[UIView class]])
    {
#ifdef DEBUG
        NSLog(@"- LAVideoPlayerCustom setControlsClass:() : Your controls must be a subclass of UIView");
#endif
        return;
    }
    
    if (self.controls)
    {
        [self.controls removeFromSuperview];
        self.controls = nil;
    }
    
    self.controls = [[controlsClass alloc] initWithPlayer:self];
    self.controls.frame = self.bounds;
    self.controls.clipsToBounds = YES;
    [self.controls setupDisplay];
    [self addSubview:self.controls];
}

#pragma clang diagnostic push

@end
