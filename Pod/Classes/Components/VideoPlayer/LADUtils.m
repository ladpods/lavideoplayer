//
//  LADUtils.m
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 19/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LADUtils.h"

@implementation LADUtils

+ (CGRect)getScreenRect
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIApplication *application = [UIApplication sharedApplication];
    UIInterfaceOrientation interfaceOrientation = application.statusBarOrientation;

    if (UIInterfaceOrientationIsLandscape(interfaceOrientation) && ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending))
    {
        size = CGSizeMake(size.height, size.width);
    }
    
    if (application.statusBarHidden == NO && ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending))
    {
        size.height -= MIN(application.statusBarFrame.size.width, application.statusBarFrame.size.height);
    }
    
    return CGRectMake(0, 0, size.width, size.height);
}

@end