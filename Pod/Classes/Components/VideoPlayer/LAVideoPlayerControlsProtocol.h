//
//  VPVideoPlayerControlsProtocol.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 18/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCKDevice;
@class LAVideoPlayerCustom;

@protocol LAVideoPlayerControlsProtocol <NSObject>

- (id)initWithPlayer:(LAVideoPlayerCustom *)videoPlayer;
- (void)setupDisplay;
- (void)refreshDisplay;

@optional

- (void)videoDidPlay;
- (void)videoDidPause;
- (void)videoDidStop;
- (void)videoDidProgress:(CGFloat)secondsElapsed;
- (void)videoDidBufferedFrom:(CGFloat)startSeconds to:(CGFloat)toSeconds;
- (void)videoMetadataLoaded:(CGFloat)duration;
- (void)hideControls:(BOOL)animated;
- (void)showControls:(BOOL)animated;
- (void)videoDidExpand;
- (void)videoDidContract;

// Chromecast
- (void)chromecastStatusUpdated;
- (void)startAnimatingChromecastButton;
- (void)stopAnimatingChromecastButton;

@end