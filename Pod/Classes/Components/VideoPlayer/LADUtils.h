//
//  LADUtils.h
//  VideoPlayer
//
//  Created by MIRAULT Fabien on 19/06/2014.
//  Copyright (c) 2014 Lagardere Active. All rights reserved.
//

#import "LAVideoPlayerConstant.h"

@interface LADUtils : NSObject

+ (CGRect)getScreenRect;

@end