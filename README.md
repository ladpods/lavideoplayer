# LAVideoPlayer

[![CI Status](http://img.shields.io/travis/Stéphane Couzinier/LAVideoPlayer.svg?style=flat)](https://travis-ci.org/Stéphane Couzinier/LAVideoPlayer)
[![Version](https://img.shields.io/cocoapods/v/LAVideoPlayer.svg?style=flat)](http://cocoapods.org/pods/LAVideoPlayer)
[![License](https://img.shields.io/cocoapods/l/LAVideoPlayer.svg?style=flat)](http://cocoapods.org/pods/LAVideoPlayer)
[![Platform](https://img.shields.io/cocoapods/p/LAVideoPlayer.svg?style=flat)](http://cocoapods.org/pods/LAVideoPlayer)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LAVideoPlayer is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LAVideoPlayer"
```

## Customization
LAVideoPlayer contains default images for play button, pause button etc. but you can customize them, here is an example:

```objectivec
LAVideoFullScreenViewController * vc = [[LAVideoFullScreenViewController alloc] initWithVideoUrl:@"http://videos.premiere.fr/1/7/6/4166176/hd-4166176.mp4" andAds:@"" andGaAction:@"" andGaLabel:@"" andGaDimensions:tmp andATSiteID:@"5" withArticle:TRUE];

[self presentViewController:vc animated:YES completion:^{
//Player images custom
LAVideoPlayerFullScreenControls * controls = [vc getControls];
[controls setBtnSliderImage:[UIImage imageNamed:@"player_now"]]; // Slider button 
[controls setBtnPlayImage:[UIImage imageNamed:@"player_play"]]; // Play button
[controls.progressLabel setFont:[UIFont boldSystemFontOfSize:10]]; // progressLabel font
[controls.durationLabel setFont:[UIFont boldSystemFontOfSize:10]]; // progressLabel font
}];
```

All functions you can use for customization :

LAVideoFullScreenViewController class :

```objectivec
- (void)setCloseButtonImage:(UIImage*)image;
```

LAVideoPlayerSimpleControls class :

```objectivec
- (void)setTopBarImage:(UIImage *)image;
- (void)setIconCloseImage:(UIImage *)image;
- (void)setBottomBarImage:(UIImage *)image;
- (void)setBackgroundSliderImage:(UIImage *)image;
- (void)setBufferingSliderImage:(UIImage *)image;
- (void)setTrackingSliderImage:(UIImage *)image;
- (void)setBtnSliderImage:(UIImage *)image;
- (void)setBtnPauseImage:(UIImage *)image;
- (void)setBtnPlayImage:(UIImage *)image;
```

## Author

Stéphane Couzinier, stephane.couzinier@lagardere-active.com

## License

LAVideoPlayer is available under the MIT license. See the LICENSE file for more info.
